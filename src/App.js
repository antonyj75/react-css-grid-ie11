import React from 'react';
import './App.scss';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <button>Back</button>
        <button>Buy</button>
      </header>
      <section className="av-check">
        <h2>Availability check not yet done.</h2>
      </section>
      <section className="overview">
        <h2>Philips Stabmixer</h2>
      </section>
      <section className="hardware">
        <img className="hardware__image" src="/logo192.png" alt="" />
        <div className="hardware__select">
          <input type="checkbox" id="hw-checkbox" />
          <label htmlFor="hw-checkbox">Scrambler</label>
        </div>
        <p className="hardware__price">2,99 € / Monat</p>
      </section>
      <section className="prod-details">
        <table className="prod-details__table">
          <caption>Vergragslaufzeiten</caption>
          <tbody>
          <tr>
            <td>Laufzeit</td>
            <td>24 Monate</td>
          </tr>
          <tr>
            <td>Kündigungsfrist</td>
            <td>1 Monat</td>
          </tr>
          </tbody>
        </table>
      </section>
      <section className="price-details">
        <h2>Detaillierte Preisübersicht</h2>
        <table className="price-details__table">
          <thead>
          <tr>
            <th>Tarifkosten</th>
            <th>Einmalig</th>
            <th>Laufend</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>Grundgebühr 1. - 6. Monat</td>
            <td></td>
            <td>0,00 €</td>
          </tr>
          <tr>
            <td>Grundgebühr 7. - 12. Monat</td>
            <td></td>
            <td>19,99 €</td>
          </tr>
          </tbody>
        </table>
      </section>
    </div>
  );
}

export default App;
